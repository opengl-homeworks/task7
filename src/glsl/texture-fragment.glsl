#version 400 core

struct AmbientLight {
    vec3 color;
};

struct DirectionalLight {
    mat4 matrix;
    vec3 color;
    vec3 direction;
};

struct PointLight {
    mat4 matrix;
    vec3 color;
    vec3 position;
    float attenuation;
    bool enabled;
};

struct SpotLight {
    mat4 matrix;
    vec3 color;
    vec3 position;
    vec3 direction;
    float innerAngleCos;
    float outerAngleCos;
    float attenuation;
    bool enabled;
};

struct Colors {
    vec3 light;
    vec3 specular;
};

vec3 calculateDirectionalLightColor(DirectionalLight directionalLight, vec3 normal);

Colors calculatePointLightColor(PointLight pointLight, vec3 cameraPosition, vec3 normal, vec3 position, float specular, float shininess);

Colors calculateSpotLightColor(SpotLight spotLight, vec3 cameraPosition, vec3 normal, vec3 position, float specular, float shininess);

uniform vec3 u_cameraPosition; // The camera position
uniform float u_specular; // The specular parameter (how much the specular lighting affects the object)
uniform float u_shininess; // The shininess parameter (how reflective the object is, opposite to roughness)
uniform AmbientLight u_ambientLight; // The ambient light parameters
uniform DirectionalLight u_directionalLight; // The directional light parameters
uniform sampler2D u_texture; // The texture
uniform sampler2DShadow u_directionalLightTexture; // The directional light shadow texture

in vec3 v_position; // The fragment position in the world space
in vec3 v_normal; // The fragment normal for lighting
in vec2 v_textureCoordinates; // The texture coordinates

out vec4 color; // The fragment color

// Calculates the fragment visibility based on the shadow map
// If you don't see shadows, try setting the bias to zero
float calculateVisibility(vec3 position, mat4 lightMatrix, sampler2DShadow lightTexture, float bias) {
    vec4 transformedPosition = lightMatrix * vec4(position, 1.0); // Calculate the fragment position in the light camera clip space
    vec3 shadowCoordinates = (transformedPosition.xyz / transformedPosition.w) / 2.0 + 0.5; // Calculate the coordinates in the light shadow texture
    shadowCoordinates.z -= bias; // Add the bias to the fragment depth to avoid the artifacts
    return texture(lightTexture, shadowCoordinates); // Compare the fragment depth with the depth stored in the texture (the value from zero to one is returned since the linear filtering is turned on)
}

void main() {
    // Make sure that the normal has a unit length after the interpolation
    vec3 normal = normalize(v_normal);

    //Calculate the light and specular colors
    vec3 lightColor = u_ambientLight.color;

    // Calculate the visibility for the directional light
    float visibility = calculateVisibility(v_position, u_directionalLight.matrix, u_directionalLightTexture, 0.05);

    lightColor += visibility * calculateDirectionalLightColor(u_directionalLight, normal);

    // Set the fragment color based on the texture and light colors
    color = texture(u_texture, v_textureCoordinates) * vec4(lightColor, 1.0);
}
