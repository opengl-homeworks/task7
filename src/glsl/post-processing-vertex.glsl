#version 400 core

layout (location = 0) in vec2 a_position; // The coordinates of the viewport corner

out vec2 v_textureCoordinates; // The texture coordinates

void main() {
    v_textureCoordinates = (a_position + 1.0) / 2.0;
    gl_Position = vec4(a_position, 0.0, 1.0);
}
