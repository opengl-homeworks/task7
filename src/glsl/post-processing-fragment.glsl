#version 400 core

uniform sampler2D u_texture; // The texture
uniform int u_effectIndex; // The effect index

in vec2 v_textureCoordinates; // The texture coordinates

out vec4 color; // The fragment color

vec4 noEffect() {
    return texture(u_texture, v_textureCoordinates);
}

vec4 grayscale() {
    vec4 t = texture(u_texture, v_textureCoordinates);
    float intensity = (t.r + t.g + t.b) / 3.0;
    return vec4(vec3(intensity), 1.0);
}

vec4 aberration() {
    return vec4(
        textureOffset(u_texture, v_textureCoordinates, ivec2(10, 10)).r,
        texture(u_texture, v_textureCoordinates).g,
        textureOffset(u_texture, v_textureCoordinates, ivec2(20, 20)).b,
        1.0
    );
}

const vec2 offset25[25] = vec2[](
    vec2(-2.0,-2.0), vec2( -1.0,-2.0), vec2( 0.0,-2.0), vec2( 1.0,-2.0), vec2( 2.0,-2.0),
    vec2(-2.0,-1.0), vec2( -1.0,-1.0), vec2( 0.0,-1.0), vec2( 1.0,-1.0), vec2( 2.0,-1.0),
    vec2(-2.0, 0.0), vec2( -1.0, 0.0), vec2( 0.0, 0.0), vec2( 1.0, 0.0), vec2( 2.0, 0.0),
    vec2(-2.0, 1.0), vec2( -1.0, 1.0), vec2( 0.0, 1.0), vec2( 1.0, 1.0), vec2( 2.0, 1.0),
    vec2(-2.0, 2.0), vec2( -1.0, 2.0), vec2( 0.0, 2.0), vec2( 1.0, 2.0), vec2( 2.0, 2.0)
);

const float blurKernel[25] = float[](
    1.0 / 256, 4.0  / 256,  6.0 / 256,  4.0 / 256, 1.0 / 256,
    4.0 / 256, 16.0 / 256, 24.0 / 256, 16.0 / 256, 4.0 / 256,
    6.0 / 256, 24.0 / 256, 36.0 / 256, 24.0 / 256, 6.0 / 256,
    4.0 / 256, 16.0 / 256, 24.0 / 256, 16.0 / 256, 4.0 / 256,
    1.0 / 256, 4.0  / 256,  6.0 / 256,  4.0 / 256, 1.0 / 256
);

vec4 blur() {
    vec2 pstep = vec2(1.0) / vec2(textureSize(u_texture, 0)) * 2; // Jump 2 pixels for better effect
    vec4 res = vec4(0.0);

    for (int i = 0; i < 25; i++) {
        res += texture(u_texture, v_textureCoordinates + offset25[i] * pstep) * blurKernel[i];
    }
    return res;
}

const float embossKernel[25] = float[](
    -2.0,  0.0, -1.0,  0.0,  0.0,
    0.0, -2.0, -1.0,  0.0,  0.0,
    -1.0, -1.0,  1.0,  1.0,  1.0,
    0.0,  0.0,  1.0,  2.0,  0.0,
    0.0,  0.0,  1.0,  0.0,  2.0
);

vec4 emboss() {
    vec2 pstep = vec2(1.0) / vec2(textureSize(u_texture, 0));
    vec4 res = vec4(0.0);

    for (int i = 0; i < 25; i++) {
        res += texture(u_texture, v_textureCoordinates + offset25[i] * pstep) * embossKernel[i];
    }
    return res;
}

vec4 inverse() {
    return vec4(1.0) - texture(u_texture, v_textureCoordinates);
}

vec4 sepia() {
    vec4 factor = vec4(0.27, 0.67, 0.06, 0.0);
    vec4 darkColor = vec4(0.2, 0.05, 0.0, 0.0);
    vec4 lightColor = vec4(1.0, 0.9, 0.5, 0.0);
    return mix(darkColor, lightColor, dot(factor, texture(u_texture, v_textureCoordinates)));
}

void main() {
    if (v_textureCoordinates.x >= 0.5) {
        color = noEffect();
    }
    else if (u_effectIndex == 1) {
        color = grayscale();
    }
    else if (u_effectIndex == 2) {
        color = aberration();
    }
    else if (u_effectIndex == 3) {
        color = blur();
    }
    else if (u_effectIndex == 4) {
        color = emboss();
    }
    else if (u_effectIndex == 5) {
        color = inverse();
    }
    else if (u_effectIndex == 6) {
        color = sepia();
    }
    else {
        color = noEffect();
    }
}
